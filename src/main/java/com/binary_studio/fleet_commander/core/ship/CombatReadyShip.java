package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private int currentHullHP;

	private int currentShieldHP;

	private int currentPower;

	private final int maxHullHP;

	private final int maxShieldHP;

	private final int maxPowerAmount;

	private final DockedShip baseShip;

	public CombatReadyShip(DockedShip ship) {
		this.baseShip = ship;
		this.currentPower = ship.getCapacitorAmount().value();
		this.currentShieldHP = ship.getShieldHP().value();
		this.currentHullHP = ship.getHullHP().value();
		this.maxHullHP = this.currentHullHP;
		this.maxShieldHP = this.currentShieldHP;
		this.maxPowerAmount = this.currentPower;
	}

	@Override
	public void endTurn() {
		int recharge = this.baseShip.getCapacitorRechargeRate().value();
		this.currentPower = Math.min(this.currentPower + recharge, this.maxPowerAmount);
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.baseShip.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.baseShip.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.baseShip.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		AttackSubsystem attackSubsystem = this.baseShip.getAttackSubsystem();
		AttackAction attackAction = null;
		int powerConsumption = attackSubsystem.getCapacitorConsumption().value();
		if (this.currentPower >= powerConsumption) {
			attackAction = new AttackAction(attackSubsystem.getBaseDamage(), this, target, attackSubsystem);
			this.currentPower -= powerConsumption;
		}
		return Optional.ofNullable(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttack = this.baseShip.getDefenciveSubsystem().reduceDamage(attack);
		if (this.currentShieldHP != 0) {
			int damagedShieldHP = this.currentShieldHP - reducedAttack.damage.value();
			this.currentShieldHP = Math.max(damagedShieldHP, 0);
			this.currentHullHP = Math.max(this.currentHullHP + Math.min(damagedShieldHP, 0), 0);
			return this.currentHullHP != 0
					? new AttackResult.DamageRecived(reducedAttack.weapon, reducedAttack.damage, this)
					: new AttackResult.Destroyed();
		}
		this.currentHullHP = Math.max(this.currentHullHP - reducedAttack.damage.value(), 0);
		return this.currentHullHP != 0
				? new AttackResult.DamageRecived(reducedAttack.weapon, reducedAttack.damage, this)
				: new AttackResult.Destroyed();

	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		DefenciveSubsystem defenciveSubsystem = this.baseShip.getDefenciveSubsystem();
		RegenerateAction regenerateAction = null;
		int powerConsumption = defenciveSubsystem.getCapacitorConsumption().value();
		int hullRegenValue = defenciveSubsystem.getHullRegeneration().value();
		int shieldRegenValue = defenciveSubsystem.getShieldRegeneration().value();
		int hullHPDifference = this.maxHullHP - this.currentHullHP;
		int shieldHPDifference = this.maxShieldHP - this.currentShieldHP;
		if (this.currentPower >= powerConsumption) {
			int regeneratedHullHP = Math.min(hullHPDifference, hullRegenValue);
			int regeneratedShieldHP = Math.min(shieldHPDifference, shieldRegenValue);
			this.currentHullHP += regeneratedHullHP;
			this.currentShieldHP += regeneratedShieldHP;
			regenerateAction = new RegenerateAction(PositiveInteger.of(regeneratedShieldHP),
					PositiveInteger.of(regeneratedHullHP));
			this.currentPower -= powerConsumption;
		}
		return Optional.ofNullable(regenerateAction);
	}

}
