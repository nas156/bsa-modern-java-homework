package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	static final int MAX_REDUCE_PERCENT = 95;

	static final double REDUCE_COEF_WHEN_MAX_PERCENT = 0.05;

	private final String name;

	private final PositiveInteger powergridRequirments;

	private final PositiveInteger capacitorConsumption;

	private final PositiveInteger impactReductionPercent;

	private final PositiveInteger shieldRegeneration;

	private final PositiveInteger hullRegeneration;

	private DefenciveSubsystemImpl(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
		this.name = name;
		this.powergridRequirments = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.impactReductionPercent = impactReductionPercent;
		this.shieldRegeneration = shieldRegeneration;
		this.hullRegeneration = hullRegeneration;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		double reducedDamage = incomingDamage.damage.value();
		Integer impactReductionPercent = this.impactReductionPercent.value();
		double impactReductionPercentCoefficient = 1 - (double) impactReductionPercent / 100;
		if (impactReductionPercent >= MAX_REDUCE_PERCENT) {
			impactReductionPercentCoefficient = REDUCE_COEF_WHEN_MAX_PERCENT;
		}
		return new AttackAction(PositiveInteger.of((int) Math.ceil(reducedDamage * impactReductionPercentCoefficient)),
				incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

	@Override
	public PositiveInteger getHullRegeneration() {
		return this.hullRegeneration;
	}

	@Override
	public PositiveInteger getShieldRegeneration() {
		return this.shieldRegeneration;
	}

}
