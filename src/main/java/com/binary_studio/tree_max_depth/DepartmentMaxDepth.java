package com.binary_studio.tree_max_depth;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		int depth = 1;
		List<Department> currentDepartments = rootDepartment.subDepartments.stream().filter(Objects::nonNull)
				.collect(Collectors.toList());
		while (!currentDepartments.isEmpty()) {
			currentDepartments = collectNotNullNestedDepartments(currentDepartments);
			depth += 1;
		}
		return depth;
	}

	public static List<Department> collectNotNullNestedDepartments(List<Department> departments) {
		return departments.stream().flatMap(x -> x.subDepartments.stream()).filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

}
